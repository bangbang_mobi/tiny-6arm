# 小型6轴桌面机械臂

#### 介绍
使用42闭环步进电机制作的小型6轴桌面机械臂，用同步带代替减速器的低成本解决方法，臂长390mm，最大负载1kg，精度0.3mm，制作成本3000元以内。可以触控屏示教，自写逆向运动算法，已支持关节运动指令和直线运动指令

制作视频讲解：
https://www.bilibili.com/video/BV1i44y1v7Ef/

## 软件架构
软件架构说明


## 安装教程

#### 1.3D模型图打开软件FreeCAD

软件下载地址：https://github.com/realthunder/FreeCAD_assembly3/releases/tag/0.11

软件教学视频：https://www.bilibili.com/video/BV1yK4y1Q7CU?p=2&spm_id_from=pageDriver

#### 2.程序打开软件stm32cubeide

## 使用说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/1024/201359_96b65233_2117144.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/103619_d3b1d00f_2117144.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/144137_985c5a77_2117144.png "屏幕截图.png")

2.  xxxx
3.  xxxx

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
